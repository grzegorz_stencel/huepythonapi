import requests
import json
import time 

class Light:
    def __init__(self, url,name, light_id):
        self.url = url
        self.name = name
        self.light_id = light_id

    @property
    def on(self):
        requests.put(self.url.format("lights/{}/state".format(self.light_id)),'{"on":true}')

    @property
    def off(self):
        requests.put(self.url.format("lights/{}/state".format(self.light_id)),'{"on":false}')

    def brightness(self,intensity):
        #hue 0 -6s5535
        requests.put(self.url.format("lights/{}/state".format(self.light_id)),'{"on":true, "sat":254, "bri":%s,"hue":10000}' % intensity)
        pass


class HueApi:
    def __init__(self, bridge_ip=None):
        if bridge_ip:
            self.bridge_ip = bridge_ip
        else:
            #naive :)
            try:
                self.bridge_ip = json.loads(requests.get("https://discovery.meethue.com").text)[0].get("internalipaddress")

                print("Bridge IP ",self.bridge_ip)
            except:
                print('Cant find bridge')

    def setup(self):
        i=1
        button_pressed = False
        username = None
        with open("username","r") as f:
            username = f.read().strip() 
        if not username:
            while not button_pressed and i<60:
                i+=1
                print(i)
                response = requests.post(
                        "http://{}/api/".format(self.bridge_ip), 
                        data=json.dumps({"devicetype":"my_hue_app#raspi"})
                        )
                if response.status_code == 200:
                    response = json.loads(response.text)
                    error = response[0].get("error")
                    success = response[0].get("success")
                    if error:
                        if error.get("description") == 'link button not pressed':
                            button_pressed = False
                            print("Press the button")
                    elif success:
                        button_pressed = True
                        username = success.get("username")
                        with open("username","w") as f:
                            f.write(username)
                time.sleep(1)
        self.username = username
        self.url = "http://{}/api/{}/{}".format(self.bridge_ip, self.username,"{}") 


    def get_lights(self):
        from pprint import pprint
        response = json.loads(requests.get(self.url.format("lights")).text)
        pprint(response)
        self.lights = {}
        for light_id, value in response.items():

            self.lights[value["name"]] = light_id
            light = Light(self.url,value["name"],light_id)
            setattr(self,value["name"] , light)
            print(light_id,value["state"],value["name"])

            
        
    #def turn_off(self,light_name):
    #    requests.put(self.url.format("lights/{}/state".format(self.lights[light_name])),'{"on":false}')

    #def turn_on(self,light_name):
    #    requests.put(self.url.format("lights/{}/state".format(self.lights[light_name])),'{"on":true}')


a = HueApi()
a.setup()
a.get_lights()
#testing
#a.hallway.on
#a.hallway.off
